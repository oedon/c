#include <stdio.h>

int more(void);

struct myStruct {
  int myNum;
  char *myLetter;
};

int main(void) {

  const int xx = 10;

  int i = 10;
  char *str = "String";
  char greeting[] = "Hello World!";
  float f = 3.14;
  double d = 8.88;
  int slice[] = {2, 4, 6, 8};
  int array[10] = {2, 4, 6, 8};

  int input;
  printf("INPUT: ");
  scanf("%i", &input);
  printf("INPUT %i\n", input);

  for (i = 0; i < 3; i++) {
    printf("SLICE %i\n", slice[i]);
  }

  int func;
  func = more();
  printf("FUNC %i\n", func);

  int point_1 = 42;
  printf("POINT 1 %p\n", &point_1);

  int *point_2 = &point_1;
  printf("POINT 2 %p\n", point_2);

  struct myStruct s1;
  s1.myLetter = "Hello Struct";
  s1.myNum = 23;

  printf("STRUCT %s %i\n", s1.myLetter, s1.myNum);
}

int more(void) { return 42; }
